<?php

namespace Tests\Unit;

use Illuminate\Support\Carbon;
use Orchestra\Testbench\TestCase;

class PlanningManagerTest extends TestCase
{
    private static $_config;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Planning\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        self::$_config = $config;

        $app['config']->set('dendev.planning.wsdl', $config['wsdl']);
        $app['config']->set('dendev.planning.login', $config['login']);
        $app['config']->set('dendev.planning.password', $config['password']);
        $app['config']->set('dendev.planning.location', $config['location']);
        $app['config']->set('dendev.planning.trace', $config['trace']);
    }

    public function testBasic()
    {
        $exist = \PlanningManager::test_me();
        $this->assertEquals('planning_manager', $exist);
    }
}
