<?php

namespace Operations;



use Orchestra\Testbench\TestCase;

final class EnseignantTest extends TestCase
{
    private static $_config;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Planning\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        self::$_config = $config;

        $app['config']->set('dendev.planning.wsdl', $config['wsdl']);
        $app['config']->set('dendev.planning.login', $config['login']);
        $app['config']->set('dendev.planning.password', $config['password']);
        $app['config']->set('dendev.planning.location', $config['location']);
        $app['config']->set('dendev.planning.trace', $config['trace']);
    }

    public function testTousLesEnseignants()
    {
        $data = \PlanningManager::tousLesEnseignants();
        $this->assertNotEmpty($data);
    }

    public function testNomEnseignant()
    {
        $key = self::$_config['key_teachers'][0];
        $name = self::$_config['name_teachers'][0];

        // ok
        $data = \PlanningManager::nomEnseignant($key);
        $this->assertEquals($name, $data);

        // ko
        $data = \PlanningManager::nomEnseignant('NOTFOUND');
        $this->assertFalse($data);
    }

    public function testNomsTableauEnseignants()
    {
        $names = self::$_config['name_teachers'];
        $keys = self::$_config['key_teachers'];

        $data = \PlanningManager::nomsTableauEnseignants($keys);
        $this->assertEquals($names[0], $data[0]);
        $this->assertEquals($names[1], $data[1]);
        $this->assertEquals($names[2], $data[2]);
        $this->assertNotEmpty($data);
    }

    public function testPrenomEnseignant()
    {
        $key = self::$_config['key_teachers'][0];
        $firstname = self::$_config['firstname_teachers'][0];

        // ok
        $data = \PlanningManager::PrenomEnseignant($key);
        $this->assertEquals($firstname, $data);

        // ko
        $data = \PlanningManager::PrenomEnseignant('NOTFOUND');
        $this->assertFalse($data);
    }

    public function testCodeEnseignant()
    {
        $key = self::$_config['key_teachers'][0];
        $code = self::$_config['code_teachers'][0];

        // ok
        $data = \PlanningManager::codeEnseignant($key);
        $this->assertEquals($code, $data);
    }

    public function testCodesTableauEnseignants()
    {
        $keys = self::$_config['key_teachers'];
        $codes = self::$_config['code_teachers'];

        $data = \PlanningManager::codesTableauEnseignants($keys);
        $this->assertEquals($codes[0], $data[0]);
        $this->assertEquals($codes[1], $data[1]);
        $this->assertEquals($codes[2], $data[2]);
        $this->assertNotEmpty($data);
    }

    public function testIcalEnseignant()
    {
        $key = self::$_config['key_teachers'][0];

        // ok
        $data = \PlanningManager::icalEnseignant($key, 52);
        $this->assertNotEmpty($data);
    }
}
