<?php

namespace Operations;

use Orchestra\Testbench\TestCase;


final class SalleTest extends TestCase
{
    private static $_config;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Planning\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        self::$_config = $config;

        $app['config']->set('dendev.planning.wsdl', $config['wsdl']);
        $app['config']->set('dendev.planning.login', $config['login']);
        $app['config']->set('dendev.planning.password', $config['password']);
        $app['config']->set('dendev.planning.location', $config['location']);
        $app['config']->set('dendev.planning.trace', $config['trace']);
    }

    public function testToutesLesSalles()
    {
        $data = \PlanningManager::toutesLesSalles();
        $this->assertNotEmpty($data);
    }

    public function testNomSalle()
    {
        $key = self::$_config['key_salles'][0];
        $name = self::$_config['name_salles'][0];

        // ok
        $data = \PlanningManager::NomSalle($key);
        $this->assertEquals($name, $data);

        // ko
        $data = \PlanningManager::NomSalle('NOTFOUND');
        $this->assertFalse($data);
    }

    public function testNomsTableauDeSalles()
    {
        $keys = self::$_config['key_salles'];
        $names = self::$_config['name_salles'];

        // ok
        $data = \PlanningManager::nomsTableauDeSalles($keys);
        $this->assertNotEmpty($data);
        $this->assertContains($names[0], $data);
        $this->assertContains($names[1], $data);

        // ko
        $data = \PlanningManager::nomsTableauDeSalles([-1]);
        $this->assertFalse($data);
    }

    public function testIcalSalle()
    {
        $key = self::$_config['key_salles'][0];

        // ok
        $data = \PlanningManager::icalSalle($key, 52);
        $this->assertNotEmpty($data);
    }
}
