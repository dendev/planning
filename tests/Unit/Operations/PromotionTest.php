<?php

namespace Tests\Unit\Operations;

use Illuminate\Support\Carbon;
use Orchestra\Testbench\TestCase;

class PromotionTest extends TestCase
{
    private static $_config;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Planning\AddonServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        self::$_config = $config;

        $app['config']->set('dendev.planning.wsdl', $config['wsdl']);
        $app['config']->set('dendev.planning.login', $config['login']);
        $app['config']->set('dendev.planning.password', $config['password']);
        $app['config']->set('dendev.planning.location', $config['location']);
        $app['config']->set('dendev.planning.trace', $config['trace']);
    }

    public function testToutesLesPromotions()
    {
        $promotions = \PlanningManager::toutesLesPromotions();
        $this->assertIsArray($promotions);
        $this->assertNotEmpty($promotions);
    }

    public function testGetAllPromotions()
    {
        $promotions = \PlanningManager::get_all_promotions();
        $this->assertIsArray($promotions);
        $this->assertNotEmpty($promotions);
    }

    public function testClePromotionEstValide()
    {
        $key = self::$_config['key_promotions'][0];
        $is_valid = \PlanningManager::clePromotionEstValide($key);

        $this->assertTrue($is_valid);
    }

    public function testNomTableauPromotions()
    {
         $keys = self::$_config['key_promotions'];
        $names = self::$_config['name_promotions'];

        // ok
        $data = \PlanningManager::nomsTableauDePromotions($keys);
        $this->assertNotEmpty($data);
        $this->assertContains($names[0], $data);
        $this->assertContains($names[1], $data);

        // ko
        $data = \PlanningManager::nomsTableauDePromotions([-1]);
        $this->assertFalse($data);
    }

     public function testNomPromotion()
    {
        $key= self::$_config['key_promotions'][0];
        $name = self::$_config['name_promotions'][0];

        $data = \PlanningManager::nomPromotion($key);
        $this->assertEquals($data, $name);
    }

     public function testCodePromotion()
    {
        $key= self::$_config['key_promotions'][0];

        $data = \PlanningManager::codePromotion($key);
        $this->assertNotFalse($data);
    }

    public function testAccederPromotionParNomEtCode()
    {
        $name = self::$_config['name_promotions'][0];
        $code = self::$_config['code_promotions'][0];
        $key = self::$_config['key_promotions'][0];

        $data = \PlanningManager::accederPromotionParNomEtCode($name, $code);
        $this->assertEquals($data, $key);
    }

    public function testIcalPromotion()
    {
        $key = self::$_config['key_promotions'][0];

        // ok
        $data = \PlanningManager::icalPromotion($key);
        $this->assertNotEmpty($data);
    }
}
