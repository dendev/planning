<?php

return [
    'wsdl' => env('HPCLIENT_WSDL'),
    'login' => env('HPCLIENT_LOGIN'),
    'password' => env('HPCLIENT_PASSWORD'),
    'location' => env('HPCLIENT_LOCATION'),
    'trace' => env('HPCLIENT_TRACE', true),
];
