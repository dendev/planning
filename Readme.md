# Planning

> Client pour utliser le soap d'hyperplanning

Permet d'utiliser les services ( certains ) soap d'hyperplanning    
Ex: J'ai besoin d'ical d'un mdp

## Installation

```bash
composer config repositories.planning vcs https://gitlab.com/dendev/planning.git
composer require dendev/planning
```

## Utilisation
La facade **PlanningManager** est disponible au sein de laravel
```php
TODO
$planning = \PlanningManager::test_me();
```

## Docs

Installer [doctum](https://github.com/code-lts/doctum)
```bash
curl -O https://doctum.long-term.support/releases/latest/doctum.phar
php doctum.phar update doctum.php
google-chrome ./docs/build/index.html
```

## Test
Executer tous les tests
```bash
phpunit 
```

Executer un jeu de test spécifique
```bash
phpunit tests/Unit/PlanningManagerTest.php
```

Executer un test spécifique
```bash
phpunit --filter testAccederPromotionParNomEtCode 
```

## Metrics

Installer [phpmetrics](https://phpmetrics.org/)
```bash
composer global require 'phpmetrics/phpmetrics'
phpmetrics --report-html=metrics ./src --junit='phpunit.xml'
google-chrome metrics/index.html
```

## ?
https://gitlab.com/help/user/packages/composer_repository/index.md
