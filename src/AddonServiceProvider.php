<?php

namespace Dendev\Planning;

use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'planning';
    protected $commands = [];
}
