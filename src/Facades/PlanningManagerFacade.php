<?php

namespace Dendev\Planning\Facades;

use Illuminate\Support\Facades\Facade;

class PlanningManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'planning_manager';
    }
}
