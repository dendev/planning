<?php

namespace Dendev\Planning\Services;


use Dendev\Hpclient\HPClient;
use Dendev\Planning\Services\Operations\Enseignant;
use Dendev\Planning\Services\Operations\Salle;
use Dendev\Planning\Services\Operations\Format;
use Dendev\Planning\Services\Operations\Promotion;
use Illuminate\Support\Carbon;

class PlanningManagerService
{
    use Enseignant;
    use Salle;
    use Promotion;
    use Format;

    private $_client;

    public function __construct()
    {
        $wsdl = config('dendev.planning.wsdl');
        $login = config('dendev.planning.login');
        $password = config('dendev.planning.password');
        $location = config('dendev.planning.location');
        $trace = config('dendev.planning.trace');
        $this->_client = new HPClient($wsdl, $login, $password, $location, $trace);
    }

    public function test_me()
    {
        return 'planning_manager';
    }
}
