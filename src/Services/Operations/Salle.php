<?php

namespace Dendev\Planning\Services\Operations;

/**
 * Travail avec les webservices salles d'hyperplanning
 * @see https://www.index-education.com/fr/ServiceWeb-Hyperplanning-Salles.php
 */
trait Salle
{
    /**
     * Récupérer les clés des salles exitantes dans HP
     *
     * @return array|false retourne une un tableau d'integer qui sont les clés des salles
     */
    public function toutesLesSalles(): array
    {
        $salles = $this->_client->toutesLesSalles();
        return $salles;
    }

    /**
     * Renvoi le nom ( colonne nom dans HP ) de la salle
     * @param $key est la clé utilisé en interne par hp pour identifié la salle
     * @return string|false le nom de la salle
     */
    public function nomSalle($key)
    {
        $nom = $this->_client->nomSalle($key);
        return $nom;
    }

    /**
     * Renvoi le nom de salle dont la clé est fournit
     * @param $keys tableau des clés
     * @return array|false le tableau des noms
     */
    public function nomsTableauDeSalles($keys)
    {
         $noms = $this->_client->nomsTableauDeSalles($keys);
        return $noms;
    }

    /**
     * Fournit l'ical de la salle
     *
     * @param $key clé de la salle
     * @param $nb_weeks integer nombre de semaines
     * @param $with ajoute ou retire de l'informations dans le resultat
     * @return string|false retourne un ical
     */
    public function icalSalle($key, $nb_weeks = 25, $with = false )
    {
        $ical = $this->_client->icalSalle($key, $nb_weeks, $with);
        return $ical;
    }
}
