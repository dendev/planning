<?php
namespace Dendev\Planning\Services\Operations;

trait Promotion
{
    use PromotionAlias;

    /**
     * Permet d'obtenir la clé d'une promotion par le nom et son code
     * @param $name nom HP
     * @param $code code HP
     * @return int clé HP
     */
    public function accederPromotionParNomEtCode($name, $code)
    {
        $promotion = $this->_client->accederPromotionParNomEtCode($name, $code);
        return $promotion;
    }

    public function clePromotionEstValide($key)
    {
        $is_valid = $this->_client->clePromotionEstValide($key);
        return $is_valid;
    }

    /**
     * Retourne les noms des promotions dont la clé est fournis
     *
     * @param $keys array des codes HP
     * @return array|false tableau des noms
     */
    public function nomsTableauDePromotions($keys)
    {
        $noms = $this->_client->nomsTableauDePromotions($keys);
        return $noms;
    }

    /**
     * Fournit le nom de la promotion dont la clé est envoyé
     * @param $key clé HP de la promotion
     * @return string|false nom de la promotion
     */
    public function nomPromotion($key)
    {
        $nom = $this->_client->nomPromotion($key);
        return $nom;
    }

    /**
     * Retourne les clés de toutes les promotions existantes dans HP
     * @return array<int> Un tableau d'entier(s) qui recense l'intégralité des clés correspondantes aux promotions.
     *
     * Vide si aucune promotion n'existe dans la base.
     */
    public function toutesLesPromotions()
    {
        $promotions = $this->_client->toutesLesPromotions();
        return $promotions;
    }

    /**
     * Fournis le code de la promotion depuis sa clé
     *
     * @param $key clé HP
     * @return int|false la clé HP
     */
    public function codePromotion($key)
    {
        $code = $this->_client->codePromotion($key);
        return $code;
    }

    /**
     * Retourne l'ical d'une promotion
     *
     * @param $key clé de la promotion
     * @param $nb_weeks int du nombre de semaines à récupérer
     * @param $with array avec les données à inclue en plus
     * @return string|false contenu de l'ical
     */
    public function icalPromotion($key, $nb_weeks = 25, $with = false)
    {
        $ical = $this->_client->icalPromotion($key, $nb_weeks, $with);
        return $ical;
    }
}
