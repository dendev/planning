<?php

namespace Dendev\Planning\Services\Operations;

/**
 * Travail avec les webservices enseignant d'hyperplanning
 */
Trait Enseignant
{
    /**
     * Retourne le code HP de l'enseignant dont la clé est fournis.
     * @param $key clé HP de l'enseignant
     * @return string|false le code de l'enseignant ex DEVDE
     */
    public function codeEnseignant($key)
    {
        $code = $this->_client->codeEnseignant($key);
        return $code;
    }

    /**
     * Retourne le code HP de l'enseignant dont la clé est fournis.
     * @param $key clé HP de l'enseignant
     * @return string|false le code de l'enseignant ex DEVDE
     */
    public function codesTableauEnseignants($key)
    {
        $code = $this->_client->codesTableauEnseignants($key);
        return $code;
    }

    /**
     * Retourne l'ical d'un enseignant
     *
     * @param $key clé HP de l'enseignant
     * @param $nb_weeks int du nombre de semaines à récupérer
     * @param $with array avec les données à inclue en plus
     * @return string|false contenu de l'ical
     */
    public function icalEnseignant($key, $nb_weeks = 25, $with = false)
    {
        $ical = $this->_client->icalEnseignant($key, $nb_weeks, $with);
        return $ical;
    }



    /**
     * Retourne le nom de l'enseignant dont la clé HP est fournis
     *
     * @param $key clé HP de l'enseignant
     * @return string|false return le nom HP
     */
    public function nomEnseignant($key)
    {
        $nom = $this->_client->nomEnseignant($key);
        return $nom;
    }

/**
     * Retourne le nom desenseignants dont la clé HP est fournis
     *
     * @param $key clé HP de l'enseignant
     * @return string|false return les noms
     */
    public function nomsTableauEnseignants($keys)
    {
        $data = $this->_client->nomsTableauEnseignants($keys);
        return $data;

        return $data;
    }

    /**
     * Retourne le prénom de l'enseignant dont la clé HP est fournis
     *
     * @param $key clé HP de l'enseignant
     * @return string|false return le prénom HP
     */
    public function prenomEnseignant($key)
    {
        $prenom = $this->_client->prenomEnseignant($key);
        return $prenom;
    }

    /**
     * Retourne la clé HP de tous les enseignants existant
     * @return array|false clés des enseignants
     */
    public function tousLesEnseignants()
    {
        $enseignants = $this->_client->tousLesEnseignants();
        return $enseignants;
    }
}

