<?php
namespace Dendev\Planning\Services\Operations;

trait PromotionAlias
{
    public function get_all_promotions()
    {
        return $this->toutesLesPromotions();
    }

    public function get_names_of_promotions($keys)
    {
        return $this->nomsTableauDePromotions($keys);
    }

    public function get_code_of_promotion($key)
    {
        return $this->codePromotion($key);
    }

    public function get_promotion_by_name_and_code($name, $code)
    {
        return $this->get_accederPromotionParNomEtCode($name, $code);
    }

    public function get_ical_of_promotion($key)
    {
        return $this->icalPromotion($key);
    }
}
