<?php
namespace Dendev\Planning\Services\Operations;

/**
 * Prend en charge les webservices  de formatage des résultats HP
 */
trait Format
{
    /**
     * Permet de changer le résultat fournis par hyperplanning en lui ajoutant ou retirant certaines informations.
     * L'appel à ce webservice va fournir une réponse numérique qui servirat à paramétrés l'appel à un autre webservice ( ex icalSalle )
     * @param $with_label désire le lable ou pas
     * @param $with_code désire le code ou pas
     * @param $with_long_label désire le lable long ou pas
     * @return integer la valeur de configuration
     */
   public function formatTexteIcal($with_label = false, $with_code = false, $with_long_label = true)
    {
        $promotions = $this->_client->formatTexteIcal($with_label, $with_code,$with_long_label);
        return $promotions;
    }

    /**
     * Permet de changer le résultat fournis par hyperplanning en lui ajoutant ou retirant certaines informations.
     * L'appel à ce webservice va fournir une réponse numérique qui servirat à paramétrés l'appel à un autre webservice ( ex icalSalle )
     *
     * @param $with_name avec ou sans le nom
     * @param $with_upper_name nom en majuscule ou pas
     * @param $with_full_firstname ajouter ou pas le prénom
     * @param $with_short_firstname prénom court
     * @param $with_code ajouter le code
     * @param $with_civility ajouter la civilité
     * @return integer la valeur de configuration
     */
    public function formatTexteIcalEnseignant($with_name = false, $with_upper_name = true, $with_full_firstname = true, $with_short_firstname = false, $with_code = false, $with_civility = false)
    {
        $value = $this->_client->formatTexteIcalEnseignant($with_name, $with_upper_name, $with_full_firstname, $with_short_firstname, $with_code, $with_civility);
        return $value;
    }
}
